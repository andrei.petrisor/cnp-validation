# cnp validation
# Rulare
php cnp.php <cnp>

#Explicatie
Format CNP: S AA LL ZZ JJ NNN C

In primul rand verificam ca stringul introdus contine numai cifre si ca are lungimea 13.

Pentru pozitia S verificam faptul ca este diferita de cifra 0.

Pentru positiile AA LL ZZ verificam ca data formata este una corecta.

Pentru pozitiile JJ verificam daca numarul format este in intervalul 1-52.

Pentru pozitiile NNN verificam sa nu fie egal cu 000.

Pentru cifra de control, verificam conform algoritmului prezentat in enunt. (i. Cifra de control este calculată după cum urmează: fiecare cifră din CNP este înmulțită cu cifra de pe aceeași poziție din numărul 279146358279; rezultatele sunt însumate, iar  ezultatul final este împărțit cu rest la 11. Dacă restul este 10, atunci cifra de control este 1, altfel cifra de control este egală cu restul.)

# Validari
In ValidatorCnp.php se fac toate verificarile necesare validarii si se intoarce un mesaj corespundator. 