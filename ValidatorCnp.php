<?php


class ValidatorCnp {

    private $cnp = array();
    const TEST_KEY = '279146358279';

    public function __construct($cnp) 
    {
        $this->cnp = $cnp;
    }

    public function validate() 
    {
        if( !$this->isNumeric() ) {
            return 'CNP invalid - nu contine doar cifre!';
        }

        if( !$this->correctLength() ) {
            return 'CNP invalid - lungime incorecta!';
        }
        
        $cnpArr = str_split($this->cnp);

        if( !$this->validateGender($cnpArr)) {
            return 'CNP invalid - Genul este invalid!';
        }

        if ( !$this->validateDate($cnpArr) ) {
            return 'CNP invalid - Data este invalida!';
        }

        if ( !$this->validateJudet($cnpArr) ) {
            return 'CNP invalid - Judetul este invalid!';
        }

        if ( !$this->validateNNN($cnpArr) ) {
            return 'CNP invalid - NNN este invalid!';
        }

        if ( !$this->validateDigitControl($cnpArr) ) {
            return 'CNP invalid!';
        }

        return 'CNP valid!';
    }

    private function isNumeric() 
    {
        if( !is_numeric($this->cnp) ) {
            return false;
        }

        return true;
    }

    private function correctLength() 
    {
        if( strlen($this->cnp) !== 13 ) {
            return false;
        }

        return true;
    }

    private function validateGender($cnpArr) 
    {
        if( $cnpArr[0] === '0' ) {
            return false;
        }

        return true;
    }

    private function validateDate($cnpArr) 
    {
        $day = intval($cnpArr[5] . $cnpArr[6]);
        $month = intval($cnpArr[3] . $cnpArr[4]);

        if( $cnpArr[0] === '1' || $cnpArr[0] === '2' || $cnpArr[0] === '7' || $cnpArr[0] === '8' || $cnpArr[0] === '9' ) {
            $year = intval('19' . $cnpArr[1] . $cnpArr[2]);
        } else {
            $year = intval('20' . $cnpArr[1] . $cnpArr[2]);
        }

        return checkdate($month, $day, $year);
    }

    private function validateJudet($cnpArr) 
    {
        $jj = intval($cnpArr[7] . $cnpArr[8]);
        if( $jj > 52 || $jj === 0 ) {
            return false;
        }

        return true;
    }

    private function validateNNN($cnpArr) 
    {
        $nnn = intval($cnpArr[9] . $cnpArr[10] . $cnpArr[11]);
        if( $nnn === 0 ) {
            return false;
        }

        return true;
    }

    private function validateDigitControl($cnpArr) 
    {
        $testKeyArr = str_split($this::TEST_KEY);
        $sum = 0;

        foreach( $testKeyArr as $key => $value ) {
            $sum += intval($value) * $cnpArr[$key];
        }

        if( (($sum % 11) === 10 && $cnpArr[12] === 1) ||
            (($sum % 11) !== 10 && $cnpArr[12] == ($sum % 11)) ) {
            return true;
        }

        return false;
    }

}